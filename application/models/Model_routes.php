<?php 

class Model_routes extends CI_Model 
{
   public function __construct()
	{
		parent::__construct();
   }
   
   /* get active routes infromation */
	public function getActiveRoutes()
	{
		$sql = "SELECT * FROM routes WHERE active = ?";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
   }
   
   /* get the routes data */
	public function getRoutesData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM routes WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM routes";
		$query = $this->db->query($sql);
		return $query->result_array();
   }
   
   //Create()
   public function create($data)
	{
		if($data) {
			$insert = $this->db->insert('routes', $data);
			return ($insert == true) ? true : false;
		}
	}

   //Update()
	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('routes', $data);
			return ($update == true) ? true : false;
		}
	}

   //Remove()
   public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('routes');
			return ($delete == true) ? true : false;
		}
	}

	// Route Counts on Dashboard
	public function countTotalRoutes()
	{
		$sql = "SELECT * FROM routes WHERE active = '1'";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

   
}