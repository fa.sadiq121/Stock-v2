<!-- <meta http-equiv="refresh" content="40;"/>  -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php if($is_admin == true): ?>
      <section class="content-header">
        <h1>
          Dashboard
          <!-- <small>Control panel</small> -->
        </h1>
        <ol class="breadcrumb">
          <!-- <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li> -->
          <!-- <li class="active">Dashboard</li> -->

        </ol>
      </section>
    <?php endif; ?>

    <?php if($is_admin == false): ?>
      <section class="content-header">
          <h1>
            Home
            <!-- <small>Control panel</small> -->
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
          
          </ol>
        </section>
    <?php endif; ?>
    

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <?php if($is_admin == true): ?>
        <div class="row">
          <div class="col-lg-3 col-xs-6">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-cubes"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Products</span>
                <span class="info-box-number"><?php echo $total_products ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="ion ion-stats-bars"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Paid Sales</span>
                <span class="info-box-number"><?php echo $total_paid_orders ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="ion ion-android-people"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Users</span>
                <span class="info-box-number"><?php echo $total_users ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-industry"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Warehouse</span>
                <span class="info-box-number"><?php echo $total_stores ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>
          <!-- ./col -->
        </div>

        <!-- 2nd Stat box -->
        <div class="row">
          <div class="col-lg-3 col-xs-6">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-code-fork"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Channels</span>
                <span class="info-box-number"><?php echo $total_channels ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-cart-plus"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Supplier</span>
                <span class="info-box-number"><?php echo $total_supplier ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-child"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Customers</span>
                <!-- <span class="info-box-number"><//?php echo $total_units ?></span> -->
                <span class="info-box-number"><?php echo $total_customers ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-truck"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Routes</span>
                <span class="info-box-number"><?php echo $total_routes ?></span>
                <!-- <span class="info-box-number">0</span> -->
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>
          <!-- ./col -->
        </div>
        <br>
        <!-- 1st table box -->
        <div class="row">
          <div class="col-md-6">
            <div class="box box-warning">
              <div class="box-header with-border">
                <h4 class="box-title">Sales</h4>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <div class="dashboard_salesbycity">
                <div class="box-body ">
                  <table id="manageTablesales" class="table ">
                    <thead>
                      <tr>
                        <th>Customer Name</th>
                        <th>Dates</th>
                        <th>Products</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="box box-warning">
              <div class="box-header with-border">
                <h4 class="box-title">Products</h4>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
              </div>
              <div class="dashboard_salesbycity">
                <div class="box-body">
                <table id="manageTableproduct" class="table  ">
                    <thead>
                      <tr>
                        <th>Product Name</th>
                        <th>Qty</th>
                      </tr>
                    </thead>
                </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->

        <!-- 2nd table box -->
        <div class="row">
          <div class="col-md-6">
            <div class="box box-warning">
              <div class="box-header with-border">
                <h4 class="box-title">Routes</h4>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <div class="dashboard_salesbycity">
                <div class="box-body ">
                  <table id="manageTableroutes" class="table ">
                    <thead>
                      <tr>
                        <th>Routes</th>
                        <th>Loads</th>
                        <th>Expenses</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="box box-warning">
              <div class="box-header with-border">
                <h4 class="box-title">Categories</h4>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
              </div>
              <div class="dashboard_salesbycity">
                <div class="box-body">
                <table id="manageTablecategory" class="table  ">
                    <thead>
                      <tr>
                        <th>Category Name</th>
                        <!-- <th>Qty</th> -->
                      </tr>
                    </thead>
                </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      <?php endif; ?>
      <?php if($is_admin == false): ?>
        <div class="row">
          <div class="col-lg-7 col-xs-12">
              <div class="info-box">
                <div class="info-box-content">
                  <h4>Welcome to DBEI Stock Inventory Management System</h4>
                   <small>Use the left side menu to navigate</small>
                </div>
              </div>
            </div>  
        </div>
      <?php endif; ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
    //$(".col-md-6").slideToggle(2500);
    var manageTablesales;
    var manageTableproduct;
    var manageTableroutes;
    var manageTablecategory;
    var base_url = "<?php echo base_url(); ?>";

    $(document).ready(function() {
      $("#dashboardMainMenu").addClass('active');

      // initialize the datatable for Sales
      manageTablesales = $('#manageTablesales').DataTable({
        'ajax': base_url + 'orders/fetchOrdersDataDashboard',
        'order': []
      });
      // initialize the datatable for Products
      manageTableproduct = $('#manageTableproduct').DataTable({
        'ajax': base_url + 'products/fetchProductDataDashboard',
        'product': []
      });
      // initialize the datatable for Routes
      manageTableroutes = $('#manageTableroutes').DataTable({
        'ajax': base_url + 'routes/fetchRoutesDataDashboard',
        'routes': []
      });
      // initialize the datatable for Category 
      manageTableroutes = $('#manageTablecategory').DataTable({
        'ajax': base_url + 'category/fetchCategoryDataDashboard',
        'category': []
      });
    });
</script>
