
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b> v1.75.08 </b>
    </div>
    &copy; <?php echo date('Y') ?> <strong>DANICA Basic Essentials Inc. </strong>
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <!-- <div class="control-sidebar-bg"></div> -->

<!-- ./wrapper -->

<script>
  $(document).ready(function(event){
    $(function() {
      $('body').removeClass('fade-out');
    });
  });
  
  $(document).keydown(function(event){
      if(event.keyCode==123){
          return false;
      }
      else if (event.ctrlKey && event.shiftKey && event.keyCode==73){        
              return false;
      }
  });

  $(document).on("contextmenu",function(e){        
    e.preventDefault();
  });

  // $('.alert').delay(5000).fadeOut(5000)


  // var selector = '.navi li';
  // $(selector).on('click', function(){
  //     $(selector).removeClass('active');
  //     $(this).addClass('active');
  // });
</script>
</body>
</html>
