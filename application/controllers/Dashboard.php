<?php 

class Dashboard extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Dashboard';
		
		$this->load->model('model_products');
		$this->load->model('model_orders');
		$this->load->model('model_users');
		$this->load->model('model_stores');
		
		$this->load->model('model_attributes');
		$this->load->model('model_routes');
		$this->load->model('model_customers');
	
	}

	/* 
	* It only redirects to the manage category page
	* It passes the total product, total paid orders, total users, and total stores information
	into the frontend.
	*/
	public function index()
	{
		$this->data['total_products'] = $this->model_products->countTotalProducts();
		$this->data['total_paid_orders'] = $this->model_orders->countTotalPaidOrders();
		$this->data['total_users'] = $this->model_users->countTotalUsers();
		$this->data['total_stores'] = $this->model_stores->countTotalStores();

		$this->data['total_channels'] = $this->model_attributes->countTotalAttriChannel();
		$this->data['total_supplier'] = $this->model_attributes->countTotalAttriSupplier();
		$this->data['total_units'] = $this->model_attributes->countTotalAttriUnits();
		$this->data['total_routes'] = $this->model_routes->countTotalRoutes();
		$this->data['total_customers'] = $this->model_customers->countTotalCustomers();

		$user_id = $this->session->userdata('id');
		// $is_admin = ($user_id == 1 || $user_id == 6 ) ? true :false;
		$is_admin = ($user_id == 1 || $user_id == 6 || $user_id == 7 ) ? true :false;

		$this->data['is_admin'] = $is_admin;
		$this->render_template('dashboard', $this->data);
	}
}